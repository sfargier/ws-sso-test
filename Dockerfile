FROM node:lts-alpine

WORKDIR /app
ADD . /app
RUN apk add --no-cache git && \
    npm install --production && \
    npm install pm2 && mkdir .pm2 && chmod 777 .pm2
ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:node_modules/.bin
ENV HOME=/app

CMD pm2 --no-daemon start -o /dev/null -e /dev/null --no-autorestart ./src/index.js
EXPOSE 8080/tcp
