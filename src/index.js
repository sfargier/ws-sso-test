const
  q = require('q'),
  _ = require('lodash'),
  express = require('express'),
  ews = require('express-ws');

const DELAY = 1000;

var app = express();
ews(app);

app.set('view engine', 'pug');
app.set('views', __dirname);

app.get('/', (req, res) => res.render('index'));

app.get('/api', (req, res) => {
  return q()
  .delay(DELAY)
  .then(() => res.status(200).send('reply'));
});

app.ws('/api', (ws, req) => {
  ws.on('message', () => {
    return q()
    .delay(DELAY)
    .then(() => ws.send('reply'));
  });
  ws.once('close', () => ws.removeAllListeners('message'));
});

app.listen(8080, () => console.log('listening on 8080'));
