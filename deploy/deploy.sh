#!/bin/sh

function die() {
    echo $1 >&2
    exit 1
}

# Create image streams
oc get istag/ws-sso-test:latest > /dev/null
if [ $? -ne 0 ]; then
    oc tag --source=docker \
        gitlab-registry.cern.ch/sfargier/ws-sso-test:latest \
        ws-sso-test:latest --scheduled=true || die 'failed to create tag'
fi

# Create apps
oc get dc/ws-sso-test > /dev/null
if [ $? -ne 0 ]; then
    oc new-app ws-sso-test:latest --name ws-sso-test || die 'failed to create resource'
    oc expose svc/ws-sso-test
fi

oc get dc/cern-sso-proxy > /dev/null
if [ $? -ne 0 ]; then
    oc new-app --template=cern-sso-proxy \
        --param=AUTHORIZED_GROUPS=CERN-Employees \
        --param=SERVICE_NAME=ws-sso-test || die 'failed to create resource'
    oc delete cm/cern-sso-proxy || die 'failed to delete cm'
    oc create configmap cern-sso-proxy --from-file=cern-sso-proxy/ || die 'failed to create cm'
    oc rollout latest cern-sso-proxy
fi
